import {GET_NAME, SET_NAME} from "../actions/name";

const initialState = {name: ''};

export default function nameReducer(state=initialState, action) {
  if (action.type === SET_NAME) {
    return {
      ...state,
      name: action.payload
    };
  } else if (action.type === GET_NAME) {
    return {
      ...state,
      name: action.payload
    };
  }  else {
    return state;
  }
}